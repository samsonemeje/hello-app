# hello-app

This project serves a Html file with "Hello World" content using an Express server.
  
To start project 

"cd express-app"

"npm start"

go to "http://localhost:3000/" in your browser.